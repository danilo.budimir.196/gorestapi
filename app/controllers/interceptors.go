package controllers

import (
	"github.com/revel/revel"
	"stanovi/app/models"
)

type Interceptors struct {
	*revel.Controller
}

func (c Interceptors) Proba() revel.Result {

	test := models.TestIntercept{"preseceno"}

	return c.RenderJSON(test)
}
