package controllers

import (
	"github.com/nfnt/resize"
	"github.com/revel/revel"
	"image"
	"image/jpeg"
	"io"
	"os"
	"stanovi/app/jwthelper"
)

type UploadConroller struct {
	*revel.Controller
}

type UploadResponse struct {
	Message string `json:"message"`
}

type Response struct {
	IsSueccsess bool   `json:"is_sueccsess"`
	ErrMessage  string `json:"err_message"`
}

func (c UploadConroller) UploadImage(img io.Reader) revel.Result {

	response := Response{}
	token := c.Request.GetHttpHeader("Authorization")
	negativeResponse := Response{false, "Doslo je do greske"}

	if _, _, ok := jwthelper.GetInstace().CheckIfLogin(token); ok {

		f, ok := c.Params.Files["img"]

		if !ok {

			return c.RenderJSON(negativeResponse)
		}

		imageFile, err := jpeg.Decode(img)
		if err == nil {

			isSavedOriginal := make(chan bool)
			isSavedResized := make(chan bool)

			go c.saveOriginalImage(imageFile, isSavedOriginal, f[0].Filename)
			go c.saveThunbImage(imageFile, isSavedResized, f[0].Filename)

			savedOriginal, savedResized := <-isSavedOriginal, <-isSavedResized

			if savedOriginal && savedResized {
				response.IsSueccsess = true
				return c.RenderJSON(response)
			} else {

				return c.RenderJSON(negativeResponse)
			}

		} else {

			return c.RenderJSON(negativeResponse)
		}

	} else {

		return c.RenderJSON(negativeResponse)

	}

}

func (c UploadConroller) saveOriginalImage(img image.Image, ok chan bool, imageName string) {

	outputFile, err := os.Create("./public/img/" + imageName)

	if err != nil {
		ok <- false
		return
	}

	if err = jpeg.Encode(outputFile, img, nil); err != nil {
		ok <- false
		return
	} else {

		ok <- true
	}

}

func (c UploadConroller) saveThunbImage(img image.Image, ok chan bool, imageName string) {

	outputFile, err := os.Create("./public/img/" + "thumbnail_" + imageName)

	if err != nil {
		ok <- false
		return
	}

	imgResized := resize.Resize(0, 250, img, resize.Lanczos3)

	if err = jpeg.Encode(outputFile, imgResized, nil); err != nil {
		ok <- false
		return
	} else {

		ok <- true
	}
}

/*func nesto()  {

	outputFile, err := os.Create("./public/img/"+f[0].Filename)
	if err != nil {
		response.IsSueccsess = false
		response.ErrMessage = "Doslo je do greske pokusajte ponovo"
		return c.RenderJSON(response)
	}else {


		if err = jpeg.Encode(outputFile,imageFile,nil); err!=nil{

			response.IsSueccsess = false
			response.ErrMessage = "Doslo je do greske pokusajte ponovo"
			return c.RenderJSON(response)

		}else {

			response.IsSueccsess = true
			return c.RenderJSON(response)

		}

	}
}*/
