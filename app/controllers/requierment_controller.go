package controllers

import (
	"database/sql"
	"fmt"
	"github.com/revel/revel"
	"stanovi/app"
	"stanovi/app/jwthelper"
	"stanovi/app/models"
)

type RequiermentController struct {
	*revel.Controller
}

func (c RequiermentController) GetRequierments() revel.Result {

	token := c.Request.GetHttpHeader("Authorization")
	var query string
	fmt.Println("token " + token)
	var rtnJson = models.RequiermentJsonToReturn{}
	if id, rolesId, ok := jwthelper.GetInstace().CheckIfLogin(token); ok {

		switch rolesId {

		case 1:
			query = "SELECT public.requirement.requirement_id,a.apartment_name, public.requirement.title, public.user.name,public.user.surname,public.user.profyle_img FROM stanovi.public.requirement JOIN public.user on requirement.user_id = public.user.user_id JOIN public.apartment a on requirement.apartment_id = a.apartment_id"
		case 2:
			query = "SELECT public.requirement.requirement_id,a.apartment_name, public.requirement.title, public.user.name,public.user.surname,public.user.profyle_img FROM stanovi.public.requirement JOIN public.user on requirement.user_id = public.user.user_id JOIN public.apartment a on requirement.apartment_id = a.apartment_id"
		case 3:
			query = "SELECT public.requirement.requirement_id,a.apartment_name, public.requirement.title, public.user.name,public.user.surname,public.user.profyle_img FROM stanovi.public.requirement JOIN public.user on requirement.user_id = public.user.user_id JOIN public.apartment a on requirement.apartment_id = a.apartment_id WHERE requirement.user_id = $1"

		}

		stmt, err := app.DB.Prepare(query)

		if err != nil {

			rtnJson.IsSuccess = false
			rtnJson.ErrMessage = err.Error()
			return c.RenderJSON(rtnJson)
		}
		defer stmt.Close()

		if rolesId != 3 {
			rtnJson = c.getAllRequierments(stmt)
			rtnJson.IsSuccess = true
			return c.RenderJSON(rtnJson)
		} else {

			rtnJson = c.getUserRequierment(stmt, id)
			rtnJson.IsSuccess = true
			return c.RenderJSON(rtnJson)
		}

	} else {

		rtnJson.IsSuccess = false
		rtnJson.ErrMessage = "Niste ulogovani"
		return c.RenderJSON(rtnJson)
	}

}

func (c RequiermentController) getAllRequierments(stmt *sql.Stmt) models.RequiermentJsonToReturn {

	var requierment = models.Requierment{}
	var rtnJson = models.RequiermentJsonToReturn{}
	rows, err := stmt.Query()

	if err != nil {
		rtnJson.IsSuccess = false
		rtnJson.ErrMessage = err.Error()
		return rtnJson
	}
	defer rows.Close()
	for rows.Next() {

		ok := rows.Scan(&requierment.RequiermentId, &requierment.ApartmentName, &requierment.Title, &requierment.UserName, &requierment.UserSurname, &requierment.UserImage)

		if ok != nil {
			rtnJson.IsSuccess = false
			rtnJson.ErrMessage = ok.Error()
			return rtnJson
		}
		rtnJson.Requierments = append(rtnJson.Requierments, requierment)

	}

	return rtnJson

}

func (c RequiermentController) getUserRequierment(stmt *sql.Stmt, id int) models.RequiermentJsonToReturn {

	var requierment = models.Requierment{}
	var rtnJson = models.RequiermentJsonToReturn{}
	rows, err := stmt.Query(id)

	if err != nil {
		rtnJson.IsSuccess = false
		rtnJson.ErrMessage = err.Error()
		return rtnJson
	}
	defer rows.Close()
	for rows.Next() {

		ok := rows.Scan(&requierment.RequiermentId, &requierment.ApartmentName, &requierment.Title, &requierment.UserName, &requierment.UserSurname, &requierment.UserImage)

		if ok != nil {
			rtnJson.IsSuccess = false
			rtnJson.ErrMessage = ok.Error()
			return rtnJson
		}

		rtnJson.Requierments = append(rtnJson.Requierments, requierment)

	}

	return rtnJson
}
