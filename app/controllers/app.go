package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"stanovi/app"
	"stanovi/app/jwthelper"
	"stanovi/app/models"
)

type App struct {
	*revel.Controller
}

func (c App) Login() revel.Result {

	var preLoginedUser models.UserPreLogin
	var loginedUser models.LoginUser
	var loginRequest models.RequestLogin
	c.Params.BindJSON(&loginRequest)
	query := "SELECT public.user.user_id,public.user.password,public.user.name,public.user.surname,public.user.profyle_img FROM public.user WHERE public.user.username = $1"

	stmt, err := app.DB.Prepare(query)
	defer stmt.Close()
	if err != nil {
		fmt.Println("eror" + err.Error() + "  " + loginRequest.Username + "fsdfdsfsdf ++")
		loginedUser.SetisSuecses(false)

		loginedUser.SetErrorMessage("Pogresili ste username")
		return c.RenderJSON(loginedUser)

	} else {
		stmt.QueryRow(loginRequest.Username).Scan(&preLoginedUser.Id, &preLoginedUser.Password, &preLoginedUser.Name, &preLoginedUser.Surname, &preLoginedUser.Img)

	}
	defer stmt.Close()
	if err := bcrypt.CompareHashAndPassword([]byte(preLoginedUser.Password), []byte(loginRequest.Password)); err != nil {
		loginedUser.SetisSuecses(false)
		loginedUser.SetErrorMessage(err.Error())
		return c.RenderJSON(loginedUser)

	} else {

		isSuecses, token := jwthelper.GetInstace().GenerateToken(preLoginedUser.Name, preLoginedUser.Id)

		if !isSuecses {
			loginedUser.SetisSuecses(true)
			loginedUser.SetNameAndSurname(preLoginedUser.Name, preLoginedUser.Surname)
			loginedUser.SetToken(token)
			loginedUser.Img = preLoginedUser.Img

			return c.RenderJSON(loginedUser)

		} else {
			loginedUser.SetisSuecses(false)
			loginedUser.SetErrorMessage("doslo je do greske pokusajte ponovo")

			return c.RenderJSON(loginedUser)

		}

	}

}

func (c App) GeneratePass() revel.Result {

	passHash, err := bcrypt.GenerateFromPassword([]byte("admin123"), 11)
	if err != nil {
		jsondata := make(map[string]interface{})
		fmt.Println(err.Error())
		jsondata["greska"] = "sranje"
		return c.RenderJSON(jsondata)
	} else {

		jsondata := make(map[string]interface{})
		jsondata["hash"] = string(passHash)
		return c.RenderJSON(jsondata)
	}

}

func (c App) ComparePass() revel.Result {
	var loginedUser models.LoginUser
	if err := bcrypt.CompareHashAndPassword([]byte("$2a$11$ovzp//aiNM4bMlHSeE.2quQJkmyIIzZCRgTGk8KCy.pNPF7EvAolG"), []byte("admin123")); err != nil {
		loginedUser.SetisSuecses(false)
		loginedUser.SetErrorMessage(err.Error())

		return c.RenderJSON(loginedUser)

	} else {
		loginedUser.SetErrorMessage("proslo je ")
		return c.RenderJSON(loginedUser)

	}

}
