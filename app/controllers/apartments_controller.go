package controllers

import (
	"database/sql"
	"fmt"
	"github.com/revel/revel"
	"stanovi/app"
	"stanovi/app/jwthelper"
	"stanovi/app/models"
	"time"
)

type UserControler struct {
	*revel.Controller
}

func (c UserControler) GetApartments() revel.Result {

	rtnJson := models.GetApartmentsResult{}

	token := c.Request.GetHttpHeader("Authorization")
	if id, rolesId, ok := jwthelper.GetInstace().CheckIfLogin(token); ok {

		var query string

		switch rolesId {
		case 1:
			query = "SELECT public.apartment.apartment_id,apartment.addres,apartment.apartment_name,apartment.time_stamp,apartment.public FROM apartment"
			break
		case 2:
			query = "SELECT public.apartment.apartment_id,apartment.addres,apartment.apartment_name,apartment.time_stamp,apartment.public FROM apartment"
			break
		case 3:
			query = "SELECT public.apartment.apartment_id, public.apartment.addres, public.apartment.apartment_name FROM public.user JOIN user_apartment ua on public.user.user_id = ua.user_id JOIN apartment  on ua.apartment_id = apartment.apartment_id WHERE public.user.user_id = $1"
			break
		}

		stmt, err := app.DB.Prepare(query)

		if err != nil {
			rtnJson.IsSuecses = false
			rtnJson.ErrMessage = "Doslo je do greske 1"
			return c.RenderJSON(rtnJson)

		} else {
			defer stmt.Close()
			if rolesId != 3 {

				if listOfApartments, ok := c.getAllApartments(stmt, err); ok {

					rtnJson.IsSuecses = true
					rtnJson.Apartments = listOfApartments
					return c.RenderJSON(rtnJson)

				} else {

					rtnJson.IsSuecses = false
					rtnJson.ErrMessage = "Doslo je do greske"
					return c.RenderJSON(rtnJson)

				}

			} else {

				if listOfApartments, ok := c.getUsersApartments(stmt, err, id); ok {

					rtnJson.IsSuecses = true
					rtnJson.Apartments = listOfApartments
					return c.RenderJSON(rtnJson)

				} else {

					rtnJson.IsSuecses = false
					rtnJson.ErrMessage = "Doslo je do greske"
					return c.RenderJSON(rtnJson)

				}

			}

		}

	} else {
		rtnJson.IsSuecses = false
		rtnJson.ErrMessage = "Niste ulogovani"
		return c.RenderJSON(rtnJson)
	}

}

func (c UserControler) getAllApartments(stmt *sql.Stmt, err error) (models.Apartments, bool) {

	apartment := models.Apartment{}
	listOfApartments := models.Apartments{}

	if err != nil {

		return listOfApartments, false

	} else {

		rows, ok := stmt.Query()

		if ok != nil {

			return listOfApartments, false

		} else {
			defer rows.Close()
			for rows.Next() {

				date := time.Time{}
				ok := rows.Scan(&apartment.ApartmentId, &apartment.Address, &apartment.ApartmentName, &date, &apartment.Public)
				if ok != nil {
					revel.INFO.Println("GetAllApartments Error", ok)
					return listOfApartments, false

				} else {

					apartment.TimeStampString = date.Format("2 Jan 2006 15:04:05")
					listOfApartments.ListOfApartments = append(listOfApartments.ListOfApartments, apartment)
					fmt.Println(apartment.ApartmentName)

				}

			}

			return listOfApartments, true

		}

	}

}

func (c UserControler) getUsersApartments(stmt *sql.Stmt, err error, id int) (models.Apartments, bool) {

	apartment := models.Apartment{}
	listOfApartments := models.Apartments{}

	if err != nil {

		return listOfApartments, false

	} else {

		rows, ok := stmt.Query(id)

		if ok != nil {

			return listOfApartments, false

		} else {
			defer rows.Close()
			for rows.Next() {

				ok := rows.Scan(&apartment.ApartmentId, &apartment.Address, &apartment.ApartmentName)
				if ok != nil {
					// fmt.Println("Parse token error ")
					return listOfApartments, false
				} else {

					listOfApartments.ListOfApartments = append(listOfApartments.ListOfApartments, apartment)
					fmt.Println(apartment.ApartmentName)

				}

			}

			return listOfApartments, true

		}

	}

}
