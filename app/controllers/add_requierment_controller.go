package controllers

import (
	"database/sql"
	"github.com/revel/revel"
	"stanovi/app"
	"stanovi/app/jwthelper"
	"stanovi/app/models"
)

type AddRequiermentController struct {
	*revel.Controller
}

func (c AddRequiermentController) GetUserApartment() revel.Result {
	rtnJson := models.ApartmentsForAddRequierment{}
	token := c.Request.GetHttpHeader("Authorization")
	if id, rolesId, ok := jwthelper.GetInstace().CheckIfLogin(token); ok {

		var query string

		switch rolesId {
		case 1:
			query = "SELECT public.apartment.apartment_id,apartment.apartment_name FROM apartment"
			break
		case 2:
			query = "SELECT public.apartment.apartment_id,apartment.apartment_name FROM apartment"
			break
		case 3:
			query = "SELECT public.apartment.apartment_id, public.apartment.apartment_name FROM public.user JOIN user_apartment ua on public.user.user_id = ua.user_id JOIN apartment  on ua.apartment_id = apartment.apartment_id WHERE public.user.user_id = $1"
			break
		}

		stmt, err := app.DB.Prepare(query)

		if err != nil {
			rtnJson.IsSuccess = false
			rtnJson.ErrMessage = "Doslo je do greske 1"
			return c.RenderJSON(rtnJson)

		} else {
			defer stmt.Close()
			if rolesId != 3 {

				if listOfApartments, ok := c.getAllApartments(stmt, err); ok {

					rtnJson.IsSuccess = true
					rtnJson.ApartmentsList = listOfApartments
					return c.RenderJSON(rtnJson)

				} else {

					rtnJson.IsSuccess = false
					rtnJson.ErrMessage = "Doslo je do greske"
					return c.RenderJSON(rtnJson)

				}

			} else {

				if listOfApartments, ok := c.getUsersApartments(stmt, err, id); ok {

					rtnJson.IsSuccess = true
					rtnJson.ApartmentsList = listOfApartments
					return c.RenderJSON(rtnJson)

				} else {

					rtnJson.IsSuccess = false
					rtnJson.ErrMessage = "Doslo je do greske"
					return c.RenderJSON(rtnJson)

				}
			}

		}

	} else {
		rtnJson.IsSuccess = false
		rtnJson.ErrMessage = "Niste ulogovani"
		return c.RenderJSON(rtnJson)
	}

}

func (c AddRequiermentController) getAllApartments(stmt *sql.Stmt, err error) (models.ApartmentForAddRequiermentList, bool) {

	apartment := models.ApartmentForAddRequierment{}
	listOfApartments := models.ApartmentForAddRequiermentList{}

	if err != nil {

		return listOfApartments, false

	} else {

		rows, ok := stmt.Query()

		if ok != nil {

			return listOfApartments, false

		} else {
			defer rows.Close()
			for rows.Next() {

				ok := rows.Scan(&apartment.Id, &apartment.ApartmentName)
				if ok != nil {
					revel.INFO.Println("GetAllApartments Error", ok)
					return listOfApartments, false

				} else {

					listOfApartments.List = append(listOfApartments.List, apartment)

				}
			}

			return listOfApartments, true
		}

	}

}

func (c AddRequiermentController) AddRequierment() revel.Result {

	token := c.Request.GetHttpHeader("Authorization")
	var requiermentId int
	positiveResponse := models.AddRequiermentResponseJson{true, ""}
	negativeResponse := models.AddRequiermentResponseJson{false, "Doslo je do greske"}

	if id, _, ok := jwthelper.GetInstace().CheckIfLogin(token); ok {

		var requierment = models.AddRequiermentJson{}
		err := c.Params.BindJSON(&requierment)
		if err != nil {
			negativeResponse.ErrMessage = err.Error()
			return c.RenderJSON(negativeResponse)
		}

		query := "INSERT INTO requirement (apartment_id,user_id,title,description) VALUES ($1,$2,$3,$4) RETURNING requirement_id"

		stmt, err := app.DB.Prepare(query)

		if err == nil {
			defer stmt.Close()
			err := stmt.QueryRow(requierment.ApartmentId, id, requierment.RequiermentTitle, requierment.RequiermentDescription).Scan(&requiermentId)

			if err == nil {

				query := "INSERT INTO requirement_image (requirement_id,img) VALUES ($1,$2)"

				for _, element := range requierment.Images {
					imageStatment, err := app.DB.Prepare(query)

					if err != nil {
						negativeResponse.ErrMessage = err.Error()
						return c.RenderJSON(negativeResponse)
					}
					_, err2 := imageStatment.Exec(requiermentId, element)

					if err2 != nil {
						negativeResponse.ErrMessage = err.Error()
						return c.RenderJSON(negativeResponse)
					}
					defer imageStatment.Close()
				}

				return c.RenderJSON(positiveResponse)

			} else {

				negativeResponse.ErrMessage = err.Error()
				return c.RenderJSON(negativeResponse)
			}

		} else {
			negativeResponse.ErrMessage = err.Error()
			return c.RenderJSON(negativeResponse)
		}

	} else {

		return c.RenderJSON(negativeResponse)

	}

}

func (c AddRequiermentController) getUsersApartments(stmt *sql.Stmt, err error, id int) (models.ApartmentForAddRequiermentList, bool) {

	apartment := models.ApartmentForAddRequierment{}
	listOfApartments := models.ApartmentForAddRequiermentList{}

	if err != nil {

		return listOfApartments, false

	} else {

		rows, ok := stmt.Query(id)

		if ok != nil {

			return listOfApartments, false

		} else {
			defer rows.Close()
			for rows.Next() {

				ok := rows.Scan(&apartment.Id, &apartment.ApartmentName)
				if ok != nil {
					// fmt.Println("Parse token error ")
					return listOfApartments, false
				} else {

					listOfApartments.List = append(listOfApartments.List, apartment)

				}

			}

			return listOfApartments, true

		}

	}

}
