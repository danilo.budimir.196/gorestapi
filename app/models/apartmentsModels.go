package models

type Apartment struct {
	ApartmentId     int    `json:"apartment_id"`
	Address         string `json:"address"`
	Public          bool   `json:"public"`
	ApartmentName   string `json:"apartment_name"`
	TimeStampString string `json:"time_stamp_string"`
}

type Apartments struct {
	ListOfApartments []Apartment
}
