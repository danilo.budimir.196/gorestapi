package models

type ApartmentForAddRequierment struct {
	Id            int    `json:"id"`
	ApartmentName string `json:"apartment_name"`
}

type ApartmentsForAddRequierment struct {
	ApartmentsList ApartmentForAddRequiermentList `json:"apartments"`
	IsSuccess      bool                           `json:"is_success"`
	ErrMessage     string                         `json:"err_message"`
}

type ApartmentForAddRequiermentList struct {
	List []ApartmentForAddRequierment `json:"list"`
}

type AddRequiermentJson struct {
	ApartmentId            int      `json:"apartment_id"`
	RequiermentTitle       string   `json:"requierment_title"`
	RequiermentDescription string   `json:"requierment_description"`
	Images                 []string `json:"images"`
}

type AddRequiermentResponseJson struct {
	IsSuccess  bool   `json:"is_success"`
	ErrMessage string `json:"err_message"`
}
