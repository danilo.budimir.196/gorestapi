package models

type UserToken struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
}

type JwtToken struct {
	Token string `json:"token"`
}

type Exeption struct {
	Message string `json:"message"`
}
