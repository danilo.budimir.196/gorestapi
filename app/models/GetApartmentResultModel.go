package models

type GetApartmentsResult struct {
	Apartments Apartments `json:"apartments"`
	IsSuecses  bool       `json:"is_suecses"`
	ErrMessage string     `json:"err_message"`
}
