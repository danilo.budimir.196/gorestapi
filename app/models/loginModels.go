package models

type LoginUser struct {
	Name       string `json:"name"`
	Surname    string `json:"surname"`
	IsSuecses  bool   `json:"login"`
	Img        string `json:"img"`
	Token      string `json:"token"`
	ErrMessage string `json:"err_message"`
}

func (l *LoginUser) SetToken(token string) {
	l.Token = token
}

func (l *LoginUser) SetisSuecses(isSuecses bool) {
	l.IsSuecses = isSuecses
}

func (l *LoginUser) SetNameAndSurname(name, surname string) {

	l.Name = name
	l.Surname = name
}

func (l *LoginUser) SetErrorMessage(message string) {
	l.ErrMessage = message
}

type UserPreLogin struct {
	Name     string
	Surname  string
	Img      string
	Id       int
	Password string
}

type RequestLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
