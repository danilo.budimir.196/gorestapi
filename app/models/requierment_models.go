package models

import "database/sql"

type Requierment struct {
	RequiermentId int            `json:"requierment_id"`
	ApartmentName string         `json:"apartment_name"`
	Title         string         `json:"title"`
	UserName      string         `json:"user_name"`
	UserSurname   string         `json:"user_surname"`
	UserImage     sql.NullString `json:"user_image"`
}

type RequiermentJsonToReturn struct {
	IsSuccess    bool          `json:"is_success"`
	Requierments []Requierment `json:"requierments"`
	ErrMessage   string        `json:"err_message"`
}
