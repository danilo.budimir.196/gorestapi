package jwthelper

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/mitchellh/mapstructure"
	"stanovi/app"
	"stanovi/app/models"
	"sync"
)

var secret = []byte("stanoviSecret")

type Token struct {
}

var instance *Token
var once sync.Once

func GetInstace() *Token {

	once.Do(func() {
		instance = &Token{}
	})

	return instance
}

func (t Token) GenerateToken(username string, id int) (bool, string) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"id":       id,
		"username": username,
	})

	tokenString, err := token.SignedString(secret)
	if err != nil {
		return true, err.Error()
	} else {

		return false, tokenString
	}

}

func (t Token) UnpackToken(tokenString string) (models.UserToken, bool) {
	var encodedToken models.UserToken
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Doslo je do greske")
		}

		return []byte(secret), nil

	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		mapstructure.Decode(claims, &encodedToken)

		fmt.Println("Unpack " + string(encodedToken.Id))
		return encodedToken, false

	} else {

		return encodedToken, true
	}

}

func (t Token) CheckIfLogin(token string) (int, int, bool) {

	var id int
	var rolesId int
	var userClaims models.UserToken
	var ok bool
	if userClaims, ok = t.UnpackToken(token); !ok {

		query := "SELECT public.user.user_id,public.user.roles_role_id FROM public.user WHERE user_id = $1"
		stmt, err := app.DB.Prepare(query)
		defer stmt.Close()
		if err != nil {
			fmt.Println(err.Error())
			return id, rolesId, false

		} else {

			stmt.QueryRow(userClaims.Id).Scan(&id, &rolesId)
			fmt.Println("claims id :" + string(userClaims.Id) + " claims username " + userClaims.Username)
			return id, rolesId, true

		}

	} else {
		return id, rolesId, false
	}

}
